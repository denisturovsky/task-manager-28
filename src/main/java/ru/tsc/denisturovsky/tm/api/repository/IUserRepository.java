package ru.tsc.denisturovsky.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.enumerated.Role;
import ru.tsc.denisturovsky.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

}